<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('oauth')->group(function () {
    Route::name('oauth.')->group(function () {
        Route::post('login', 'LoginController@login')->name('login');

        Route::get('personal-access-tokens', 'PersonalAccessTokenController@index');
        Route::post('personal-access-tokens', 'PersonalAccessTokenController@store');
        Route::delete('personal-access-tokens/{token_id}', 'PersonalAccessTokenController@destroy');
        Route::get('scopes', 'PersonalAccessTokenController@scopes');

        Route::get('tokens', 'TokenController@index');
        Route::delete('tokens/{token_id}', 'TokenController@destroy');

        Route::get('clients', 'ClientController@index');
        Route::post('clients', 'ClientController@store');
        Route::put('clients/{client_id}', 'ClientController@update');
        Route::delete('clients/{client_id}', 'ClientController@destroy');

        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::get('redirect', 'AuthorizationController@redirect')->name('redirect');
        Route::get('callback', 'AuthorizationController@callback')->name('callback');

        Route::as('key.')->group(function () {
            Route::get('redirect/key', 'ProofKeyController@redirect')->name('redirect');
            Route::get('callback/key', 'ProofKeyController@callback')->name('callback');
        });

        Route::as('grant.')->group(function () {
            Route::get('login', 'PasswordGrantController@loginForm')->name('login');
            Route::post('request/grant', 'PasswordGrantController@request')->name('request');
        });
    });
});
