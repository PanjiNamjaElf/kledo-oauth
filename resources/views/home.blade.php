@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body text-center">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <a href="{{ route('oauth.redirect') }}" class="btn btn-success" target="_blank">Request Token</a>

                        <a href="{{ route('oauth.key.redirect') }}" class="btn btn-success" target="_blank">Proof Key Request</a>

                        <a href="{{ route('oauth.grant.login') }}" class="btn btn-success" target="_blank">Password Grant Request</a>
                    </div>
                </div>

                <passport-clients style="margin-top: 15px;"></passport-clients>

                <passport-authorized-clients style="margin-top: 15px;"></passport-authorized-clients>

                <passport-personal-access-tokens style="margin-top: 15px;"></passport-personal-access-tokens>
            </div>
        </div>
    </div>
@endsection
