<?php

namespace App\Http\Controllers;

use App\Rules\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ClientController extends Controller
{
    /**
     * The redirect validation rule.
     *
     * @var Redirect
     */
    protected Redirect $redirectRule;

    /**
     * ClientController constructor.
     *
     * @param  Redirect  $redirectRule
     */
    public function __construct(Redirect $redirectRule)
    {
        $this->redirectRule = $redirectRule;
    }

    public function index(Request $request)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $token = $request->session()->get('token');

        return Http::withToken($token)->get($this->host.'/clients');
    }

    public function store(Request $request)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $request->validate([
            'name'         => 'required|max:191',
            'redirect'     => ['required', $this->redirectRule],
            'confidential' => 'boolean',
        ]);

        $token = $request->session()->get('token');

        return Http::withToken($token)->post($this->host.'/clients', $request->toArray());
    }

    public function update(Request $request, $clientId)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $request->validate([
            'name'     => 'required|max:191',
            'redirect' => ['required', $this->redirectRule],
        ]);

        $token = $request->session()->get('token');

        return Http::withToken($token)->put($this->host.'/clients/'.$clientId, $request->toArray());
    }

    public function destroy(Request $request, $clientId)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $token = $request->session()->get('token');

        return Http::withToken($token)->delete($this->host.'/clients/'.$clientId);
    }
}
