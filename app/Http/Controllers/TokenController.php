<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TokenController extends Controller
{
    public function index(Request $request)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $token = $request->session()->get('token');

        return Http::withToken($token)->get($this->host.'/tokens');
    }

    public function destroy(Request $request, $tokenId)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $token = $request->session()->get('token');

        return Http::withToken($token)->delete($this->host.'/tokens/'.$tokenId);
    }
}
