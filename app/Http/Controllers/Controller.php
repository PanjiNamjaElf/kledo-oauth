<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected string $host = "http://app.kledo.test/api/v1/oauth";

    protected function unauthenticated(): \Illuminate\Http\JsonResponse
    {
        return \response()->json([
            'message' => 'Unauthenticated',
            'code'    => 401,
        ]);
    }
}
