<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    protected string $host = "http://app.kledo.test/api/v1";

    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = Str::random('5');
        $remember = $request->filled('remember') ? 1 : 0;

        $response = Http::asJson()
            ->post($this->host.'/authentication/singleLogin', [
                'email'       => $email,
                'password'    => $password,
                'remember_me' => $remember,
            ]);

        if ($response->successful() && isset($response['data'])) {
            $token = $response['data']['data']['access_token'];

            $request->session()->put('token', $token);

            return \redirect()->route('home');
        }

        return \redirect()->back()
            ->withStatus('Failed to login.');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect()->route('login');
    }
}
