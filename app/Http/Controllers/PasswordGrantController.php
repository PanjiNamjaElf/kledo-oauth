<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PasswordGrantController extends Controller
{
    private string $clientId = '12';
    private string $clientSecret = 'NlhPIFzxzZQcFvMtRSfDmCfm2FrSrFqkmxPmEVOH';

    public function loginForm()
    {
        return view('login');
    }

    public function request(Request $request)
    {
        $request->validate([
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        return Http::post($this->host.'/token', [
            'grant_type'    => 'password',
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'username'      => $request->input('email'),
            'password'      => $request->input('password'),
            'scope'         => '',
        ]);
    }
}
