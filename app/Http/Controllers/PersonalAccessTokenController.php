<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PersonalAccessTokenController extends Controller
{
    public function index(Request $request)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $token = $request->session()->get('token');

        return Http::withToken($token)->get($this->host.'/personal-access-tokens');
    }

    public function store(Request $request)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $scopes = $this->scopes($request);
        $scopes = \collect($scopes)->pluck('id')->values()->all();

        $request->validate([
            'name'   => 'required|max:191',
            'scopes' => 'array|in:'.implode(',', $scopes),
        ]);

        $token = $request->session()->get('token');

        return Http::withToken($token)
            ->post($this->host.'/personal-access-tokens', $request->toArray());
    }

    public function destroy(Request $request, $tokenId)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $token = $request->session()->get('token');

        return Http::withToken($token)->delete($this->host.'/personal-access-tokens/'.$tokenId);
    }

    public function scopes(Request $request)
    {
        if (! $request->session()->has('token')) {
            return $this->unauthenticated();
        }

        $token = $request->session()->get('token');

        return Http::withToken($token)->get($this->host.'/scopes');
    }
}
